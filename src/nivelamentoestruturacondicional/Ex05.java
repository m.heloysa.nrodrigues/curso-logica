package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Ex05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("digite o nome do aluno");
        String nome = scanner.nextLine();

        System.out.println("digite um numero");
        Double n1 = scanner.nextDouble();

        System.out.println("digite um numero");
        Double n2 = scanner.nextDouble();

        System.out.println("digite um numero");
        Double n3 = scanner.nextDouble();

        Double media = ((n1 + n2 + n3) / 3);
        if (media >= 6) {
            System.out.println(" O aluno " + nome + " esta aprovado");
        } else {
            System.out.println(" O aluno " + nome + " esta reprovado");
        }
    }
}
