package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Ex08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("digite o numero");
        Double numero = scanner.nextDouble();


        if ((numero > 0) && (numero < 10)) {

            System.out.println("unidade");
        }
        if ((numero > 9) && (numero < 100)) {

            System.out.println("dezena");
        }
        if ((numero > 99)) {

            System.out.println("unidade");
        }
    }}