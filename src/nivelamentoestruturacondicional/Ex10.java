package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Ex10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("digite o idade");
        Double idade = scanner.nextDouble();


        if (idade < 16 ) {

            System.out.println("nao pode votar");
        }
        if ((idade > 15) && (idade < 18)) {

            System.out.println("voto opcional");
        }
        if ((idade > 17) && (idade < 61)) {

            System.out.println("voto obrigatorio");
        }
        if ((idade > 60)) {

            System.out.println("voto obrigatorio");
        }
    }
}