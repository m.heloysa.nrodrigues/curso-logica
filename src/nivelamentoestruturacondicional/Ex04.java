package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Ex04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite a operacao + - / *");
        String operacao = scanner.nextLine();

        System.out.println("Digite de a");
        Double a = scanner.nextDouble();

        System.out.println("Digite de b");
        Double b = scanner.nextDouble();
        if (operacao.equals("+")) {
            System.out.println(a + b);
        } else if (operacao.equals("-")) {
            System.out.println(a - b);
        } else if (operacao.equals("*")) {
            System.out.println(a * b);
        } else if (operacao.equals("/")) {
            System.out.println(a / b);
        }
    }
}
