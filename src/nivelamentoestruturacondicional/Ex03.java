package nivelamentoestruturacondicional;

import java.util.Scanner;

public class Ex03 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite um numero");
        Double numero = scanner.nextDouble();

        if (numero < 0) {
            System.out.println(numero * -1);
        } else {
            System.out.println(numero);
        }
    }
}
